import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-redux'
import store from '../../store'
import Header from '../../components/header/Header'

class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <div>
                   <Header/>
                </div>
            </Provider>
        )
    }
}

export default Root