import React, { Component, PropTypes } from 'react'
import DevTools from './DevTools'
import { Provider } from 'react-redux'
import store from '../../store'
import Header from '../../components/header/Header';
import Navigation from '../../components/navigation/Navigation';

class Root extends Component {
  state = {
    showClass: 'is-show'
  };

  static childContextTypes = {
      showClass: PropTypes.string
  };

  getChildContext(){
    return {
      showClass: this.state.showClass
    }
  }

    render() {
        return (
            <Provider store={store}>
                <div className="l-container">
                    <Header/>
                    <Navigation/>
                    <DevTools />
                    {this.props.children}
                </div>
            </Provider>
        )
    }
}

export default Root