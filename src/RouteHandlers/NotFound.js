import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

class NotFound extends Component {
    static propTypes = {

    };

    render() {
        return (
            <div>
                <h1>Page not found, try exploring:
                  <Link to = {'/page1'}>go to index page</Link>
                </h1>
            </div>
        )
    }
}

export default NotFound