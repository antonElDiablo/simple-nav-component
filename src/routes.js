import React from 'react'
import { Route, Router, IndexRoute, IndexRedirect, browserHistory, hashHistory } from 'react-router'
import Root from './RouteHandlers/Root';
import Page1 from './RouteHandlers/page1/Page1';
import Page2 from './RouteHandlers/page2/Page2';
import Page3 from './RouteHandlers/page3/Page3';

import NotFound from './RouteHandlers/NotFound'
export default (
    <Router history = {hashHistory}>
        <Route path = "/" component = {Root}>
            <Route path = '/page1' component = {Page1}></Route>
            <Route path = '/page2' component = {Page2}></Route>
            <Route path = '/page3' component = {Page3}></Route>
        </Route>
        <Route path = "*" component = {NotFound}/>
    </Router>
)