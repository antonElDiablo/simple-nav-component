import { createStore, compose, applyMiddleware } from 'redux';
import reducer from '../reducers';
import multi from 'redux-multi';
import thunk from 'redux-thunk';
const commonMiddlewares = [applyMiddleware(multi, thunk)];
const envMiddlewares = require('./dev').default;

const enhancer = compose(...commonMiddlewares.concat(envMiddlewares));

const store = createStore(reducer, {}, enhancer);

//for debug only, no need in production
window.store = store;


export default store